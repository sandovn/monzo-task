package crawler

import (
    "fmt"
    "github.com/stretchr/testify/mock"
    "log"
    "monzo-task/app/internal/config"
    "monzo-task/app/internal/crawler"
    "monzo-task/app/internal/storage/entities"
    "net/http"
    "net/http/httptest"
    "testing"
)

func TestPage_GetUrl(t *testing.T) {
    expectedUrl := "https://monzo.com"
    page := entities.NewPage(expectedUrl)

    if page.Url != expectedUrl {
        t.Errorf("Page.GetHash() = %v, want %v", page.Url, expectedUrl)
    }
}

func TestPage_GetHash(t *testing.T) {
    page := entities.NewPage("https://monzo.com")
    expectedHash := "81146cd6051bbfc62fe29dac5b078e11"

    if page.Hash != expectedHash {
        t.Errorf("Page.GetHash() = %v, want %v", page.Hash, expectedHash)
    }
}

type StorageMock struct {
    mock.Mock
}

func (gs StorageMock) StoreNode(node entities.Node) {
}

func (gs StorageMock) AddRelation(nodeFrom entities.Node, nodeTo entities.Node, relation entities.Relation) {
}

func (gs StorageMock) Close() {
}

func TestCrawler_Run(t *testing.T) {
    baseConfig := config.Configuration{FixedDomainCrawling:true}
    var client = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintln(w, "TestClient")
    })).Client()
    storage := new(StorageMock)
    crawler := crawler.NewWebCrawler("https://monzo.com", client, storage, baseConfig)

    crawler.Run()

    log.Print(crawler)
}
