package main

import (
    "flag"
    "log"
    "monzo-task/app/internal/config"
    "monzo-task/app/internal/crawler"
    "monzo-task/app/internal/storage"
    "net/http"
    "time"
)

func main() {
    startTime := time.Now()

    printSitemap := flag.Bool("print-sitemap", false, "Print sitemap pages and links")
    flag.Parse()

    conf, err := config.LoadConfig("app/config/config.json")
    if err != nil {
       panic(err)
    }

    graphStorage := storage.NewGraphStorage(conf)
    defer graphStorage.Close()

    if !*printSitemap {
        startUrl := "https://monzo.com"

        webCrawler := crawler.NewWebCrawler(startUrl, new(http.Client), graphStorage, conf)
        webCrawler.Run()
    } else {
        graphStorage.FetchAllNodes()
    }

    log.Println("Crawler run time: ", time.Since(startTime))
}
