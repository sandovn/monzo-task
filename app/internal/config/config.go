package config

import (
    "encoding/json"
    "io/ioutil"
)

type Configuration struct {
    Host string
    FixedDomainCrawling bool
    MaxPages int
    Neo4jHost string
    ParallelPageWorkers int
    ParallelPageProcessing bool
}

func SaveConfig(c Configuration, filename string) error {
    bytes, err := json.MarshalIndent(c, "", "  ")
    if err != nil {
        return err
    }

    return ioutil.WriteFile(filename, bytes, 0644)
}

func LoadConfig(filename string) (Configuration, error) {
    bytes, err := ioutil.ReadFile(filename)
    if err != nil {
        return Configuration{}, err
    }

    var c Configuration
    err = json.Unmarshal(bytes, &c)
    if err != nil {
        return Configuration{}, err
    }

    return c, nil
}
