package unique_queue

import (

"github.com/orcaman/concurrent-map"
"github.com/phf/go-queue/queue"
"monzo-task/app/internal/storage/entities"

)

type UniqueQueue struct {
    *queue.Queue
    hashMap cmap.ConcurrentMap
}

func New() *UniqueQueue {
    return &UniqueQueue{queue.New(),cmap.New()}
}

func (uq *UniqueQueue) TotalPagesProcessed() int {
    return uq.hashMap.Count()
}

func (uq *UniqueQueue) PushFront(page entities.Page) {
    if uq.canAdd(page) {
        uq.hashMap.Set(page.Hash, true)
        uq.Queue.PushFront(page)
    }
}

func (uq *UniqueQueue) PushBack(page entities.Page) {
    if uq.canAdd(page) {
        uq.hashMap.Set(page.Hash, true)
        uq.Queue.PushBack(page)
    }
}

func (uq *UniqueQueue) canAdd(page entities.Page) bool {
    _, exists := uq.hashMap.Get(page.Hash)
    return !exists
}
