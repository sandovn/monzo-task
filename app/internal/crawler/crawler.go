package crawler

import (
    "github.com/PuerkitoBio/goquery"
    "github.com/PuerkitoBio/purell"
    "log"
    "monzo-task/app/internal/config"
    "monzo-task/app/internal/storage"
    "monzo-task/app/internal/storage/entities"
    queue "monzo-task/app/internal/unique-queue"
    "net/http"
    "net/url"
)

/**
This is the main interface of the Crawler. The implementation
below is capable of running in two modes: Sequential and in Parallel.
The mode is configurable through the main JSON configuration and also
if Parallel is enabled you can specify the number of worker threads to
spawn currently set to 10.

The Neo4j client is not safe and I am creating separate client for each
goroutine to make sure they don't clash when writing the the DB though
I have seen few race conditions and the Neo4j client panics if that happen
and you see an error while running the main command just rerun, it's been
built in a such way that if there are any pages that already exist in the DB
the records will be merged and you won't get duplicates.
*/
type Crawler interface {
    Run()
}

type WebCrawler struct {
    client      http.Client
    config      config.Configuration
    pageBaseUrl *url.URL
    storage     storage.Storage
    pageQueue   *queue.UniqueQueue
    brokenLinks []string
}

func NewWebCrawler(baseUrl string, client *http.Client, storage storage.Storage, config config.Configuration) *WebCrawler {
    webCrawler := new(WebCrawler)
    webCrawler.client = *client
    webCrawler.config = config
    webCrawler.storage = storage
    webCrawler.brokenLinks = make([]string, 0)

    baseUrl, err := purell.NormalizeURLString(baseUrl, purell.FlagsUnsafeGreedy)
    if err != nil {
        panic(err)
    }

    parsedBaseUrl, err := url.Parse(baseUrl)
    if err != nil {
        panic(err)
    }

    webCrawler.pageBaseUrl = parsedBaseUrl
    webCrawler.pageQueue = queue.New()

    return webCrawler
}

func (wc WebCrawler) Run() {

    basePage := entities.NewPage(wc.pageBaseUrl.String())
    wc.processPage(basePage)

    if wc.config.ParallelPageProcessing {
        wc.startParallelProcessing()
    } else {
        wc.startSequentialProcessing()
    }
}

func (wc WebCrawler) startSequentialProcessing() {
    for wc.pageQueue.Len() > 0 {

        item := wc.pageQueue.PopBack()
        page, ok := item.(entities.Page)
        if !ok {
            log.Fatal("Queue item is not of type Page", item)
            panic("Queue item is not of type Page")
        }

        wc.processPage(page)
    }
}

func (wc WebCrawler) startParallelProcessing() {
    ch := make(chan entities.Page)
    for i := 0; i < wc.config.ParallelPageWorkers; i++ {
        // this is a very ugly hack but neo4j clients are not thread safe
        // so each goroutine should have separate connection
        wc.storage = storage.NewGraphStorage(wc.config)
        go wc.processParallelPage(ch)
    }
    for wc.pageQueue.Len() > 0 {

        item := wc.pageQueue.PopBack()
        page, ok := item.(entities.Page)
        if !ok {
            log.Fatal("Queue item is not of type Page", item)
            panic("Queue item is not of type Page")
        }

        ch<- page
    }
    close(ch)
}

func (wc WebCrawler) processParallelPage(ch <-chan entities.Page) {
    for page := range ch {
        wc.processPage(page)
    }
}

func (wc WebCrawler) processPage(page entities.Page) {
    if wc.visitPage(page) {
        links := wc.getLinks(page)
        linkedPages := wc.linksToPages(links)

        relation := new(entities.LinkToPage)
        for _, subPage := range linkedPages {
            if page.Hash == subPage.Hash || !wc.visitPage(subPage) {
                continue
            }

            wc.pageQueue.PushFront(subPage)
            wc.storage.StoreNode(&subPage)
            wc.storage.AddRelation(&page, &subPage, relation)
        }

        log.Print(page, wc.pageQueue.Len(), wc.pageQueue.TotalPagesProcessed())
    }
}

func (wc WebCrawler) visitPage(page entities.Page) bool {
    pageUrl, err := url.Parse(page.Url)
    if err != nil {
        return false
    }

    if wc.config.FixedDomainCrawling {
        if wc.pageBaseUrl.Host == pageUrl.Host {
            return true
        }

        return false
    }

    return true
}

func (wc WebCrawler) linksToPages(links []string) []entities.Page {
    var pages []entities.Page
    for _, link := range links {
        page := entities.NewPage(link)
        pages = append(pages, page)
    }

    return pages
}

func (wc WebCrawler) getLinks(page entities.Page) []string {
    // Request the HTML page.
    res, err := wc.client.Get(page.Url)
    if err != nil {
        log.Fatal(err)
    }
    defer res.Body.Close()
    if res.StatusCode != 200 {
        log.Printf("status code error: %d %s", res.StatusCode, res.Status)
        if res.StatusCode == 404 {
            log.Printf("Broken links: %s", page.Url)
        }
        return nil
    }

    // Load the HTML document
    doc, err := goquery.NewDocumentFromReader(res.Body)
    if err != nil {
        log.Fatal(err)
    }

    var hrefs []string
    doc.Find("a[href]").Each(func(index int, item *goquery.Selection) {
        href, _ := item.Attr("href")
        href, err := purell.NormalizeURLString(href, purell.FlagsUnsafeGreedy)
        if err != nil {
            panic(err)
        }

        parsedUrl, err := url.Parse(href)
        if err != nil {
            panic(err)
        }
        if !parsedUrl.IsAbs() {
            href = wc.pageBaseUrl.ResolveReference(parsedUrl).String()
        }

        hrefs = append(hrefs, href)
    })

    return hrefs
}
