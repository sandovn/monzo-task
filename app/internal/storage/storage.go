package storage

import (
    "fmt"
    bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"
    "github.com/johnnadratowski/golang-neo4j-bolt-driver/structures/graph"
    "log"
    "monzo-task/app/internal/config"
    "monzo-task/app/internal/storage/entities"
)

type Config struct {
    boltUrl string
}

func NewConfig(boltUrl string) Config {
    return Config{boltUrl:boltUrl}
}

type Storage interface {
    StoreNode(node entities.Node)
    AddRelation(nodeFrom entities.Node, nodeTo entities.Node, relation entities.Relation)
    FetchAllNodes()
    Close()
}

type GraphStorage struct {
    conn bolt.Conn
}

func NewGraphStorage(config config.Configuration) GraphStorage {
    conn, err := bolt.NewDriver().OpenNeo(config.Neo4jHost)
    if err != nil {
        panic(err)
    }

    return GraphStorage{conn:conn}
}

func (gs GraphStorage) StoreNode(node entities.Node) {
    query, params := entities.NodeToQuery(node, "n")
    _, err := gs.conn.ExecNeo("MERGE " + query, params)

    if err != nil {
        panic(err)
    }
}

func (gs GraphStorage) AddRelation(nodeFrom entities.Node, nodeTo entities.Node, relation entities.Relation) {
    fromPrefix, toPrefix := "nf", "nt"

    fromQuery, fromParams := entities.NodeToQuery(nodeFrom, fromPrefix)
    toQuery, toParams := entities.NodeToQuery(nodeTo, toPrefix)
    for key, val := range toParams {
        fromParams[key] = val
    }

    query := fmt.Sprintf("MATCH %s, %s MERGE (%s)-[:%s]->(%s)", fromQuery, toQuery, fromPrefix, relation.GetType(), toPrefix)
    _, err := gs.conn.ExecNeo(query,fromParams)
    if err != nil {
        panic(err)
    }
}

func (gs GraphStorage) FetchAllNodes() {
    query := "MATCH (p:Page)-[:LINK]->(lp:Page) RETURN p, lp ORDER BY p.url ASC"
    data, rowsMetadata, _, err := gs.conn.QueryNeoAll(query, nil)
    if err != nil {
        panic(err)
    }

    log.Print(rowsMetadata)

    parentPageUrl := ""
    for _, row := range data {
        fromNode := row[0].(graph.Node)
        fromPageUrl := fromNode.Properties["url"].(string)

        toNode := row[1].(graph.Node)
        toPageUrl := toNode.Properties["url"].(string)

        if parentPageUrl != fromPageUrl {
            parentPageUrl = fromPageUrl
            fmt.Println("Parent page: ", fromPageUrl, "\nLinks: ")
        }

        fmt.Println("\t\t - ", toPageUrl)
    }
}

func (gs GraphStorage) Close() {
    if gs.conn != nil {
        gs.conn.Close()
    }
}
