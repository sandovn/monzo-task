package entities

import (
    "crypto/md5"
    "encoding/hex"
    "encoding/json"
    "fmt"
    "github.com/fatih/structs"
    "strings"
)

func NodeToQuery(node Node, label string) (string, map[string]interface{}) {
    nodeFields := structs.Fields(node)

    queryElements := make([]string, len(nodeFields))
    for i, field := range nodeFields {
        name := field.Name()
        queryElements[i] = fmt.Sprintf("%s: {%s_%s}", strings.ToLower(name), label, name)
    }

    queryFormat := strings.Join(queryElements, ", ")
    query := fmt.Sprintf("(%s:%s {%s})", label, node.GetType(), queryFormat)

    params := make(map[string]interface{})
    for key, val := range structs.Map(node) {
        params[label+"_"+key] = val
    }

    return query, params
}

type Node interface {
    GetType() string
    GetData() string
}

type Relation interface {
    GetType() string
}

type LinkToPage struct {
}

func (ltp *LinkToPage) GetType() string {
    return "LINK"
}

type Page struct {
    Url string `json:"url"`
    Hash string `json:"hash"`
}

func NewPage(url string) Page {
    hasher := md5.New()
    hasher.Write([]byte(url))
    hash := hex.EncodeToString(hasher.Sum(nil))

    return Page{Url: url, Hash: hash}
}

func (p *Page) GetType() string {
    return "Page"
}

func (p *Page) GetData() string {
    data, err := json.Marshal(p)
    if err != nil {
        panic(err)
    }

    return string(data)
}
