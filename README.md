Monzo Web Crawler
===========================

Requirements
------------
1. docker
2. docker-compose

**Note:** On a Mac you can install the above via [brew](http://brew.sh/)

Setup
-----
1. Run `docker-compose build`
2. Run `docker-compose up`

Reasoning
-----
#### Functionality

The main interface of the Crawler is located in the `crawler.go` file. The implementation is capable of running 
in two modes: Sequential and in Parallel. The mode is configurable through the main JSON configuration and also
if Parallel is enabled you can specify the number of worker threads to spawn currently set to 10.

On first run the script will start from the home page of Monzo and retrieve page by page all links. Those links
will be stored in a special data structure that is backed by Queue to where we push/pop links and ConcurrentMap 
so we can execute the process in parallel and also prevent duplicate entries in the DB, so once an entry has been 
processed in the queue it won't be added second time. 

To identify Pages I am using an MD5 hash of the URL this is probably not best option as if the URL has variable 
query parameters single Page can be processed and stored multiple times.

All of the pages are stored in graph database (Neo4j) and each Node has directional links to sub-pages that appear
in the HTML. The reason to use this DB was it is lightweight NoSLQ option and also provides easy access to all links
and related pages. As an added bonus one can generate complex paths and see full picture of the website and the relation 
between the pages.

The generation of the sitemap fetches all nodes and walks through each of them and prints each node's linked pages.       

**NOTE:** The Neo4j client is not thread safe and I am creating separate client for each goroutine to make sure 
they don't clash when writing the the DB, though I have seen few race conditions and the Neo4j client panics if 
that happen and you see an error while running the main command just rerun, it's been built in a such way that 
if there are any pages that already exist in the DB the records will be merged and you won't get duplicates.

#### Improvements
To make this fully functional/production ready crawler there are few things that need to be added/improved:
1. Improve test coverage 
2. Add configuration for Proxies instead of doing direct connection 
3. Allow base crawling domain to be passed as command argument 
4. Add better error handling on the inputs and unexpected responses
5. Add HTTP Client timeouts and better handling of HTTP errors

#### General usage

The application is built in such way that when you run the docker compose it will deploy Neo4j database
the GO app will be built and after that will run the crawler on Monzo's website. Once the crawling has 
completed it will automatically run second command to fetch all pages and build a sitemap.

If you want to build the project manually you have to run following commands:

```bash
# deploy a Neo4j server locally
docker run --publish=7474:7474 --publish=7687:7687 --volume=$HOME/neo4j/data:/data neo4j
# build go app from source 
go build -o main app/cmd/monzo/main.go
# run crawler first to load all data to the db
./main
# run the sitemap generation
./main --print-sitemap
```

#### Neo4j database usage
The main database used for the project is [Neo4j](https://neo4j.com/) and it also comes with web UI that can be reached here
[http://localhost:7474/browser/](http://localhost:7474/browser/).


Testing
--------------
I had an attempt to add test coverage but had struggled with GO patterns for mocking and general test suite.
Generally my approach would be to write test and make them pass, mock external dependencies for unit tests 
and probably add few Integration end-to-end tests.  

Service Layout
--------------
1. GO container, it will be used to build the app from source and run 2 commands (crawling/sitemap generation) 
2. Neo4J graph database, that is used to hold the data and do the path mapping for the Pages and their Links
